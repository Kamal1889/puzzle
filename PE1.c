#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/*
    @author Kamaljit Grewal
    My solution created for course EECS 2031. Problem designed by Professor Marzieh Ahmadzadeh.
    
    Like a word search, but with numbers. This code searches a 2D grid left to right, top to bottom, right to left, and bottom to top
    for instances of a given input. Return the number of times the input was found in the grid.
*/

// please do not touch the following 4 lines.
int SIZE; 
void setSize(int size){
    SIZE = size;
}
//assume size >= 2


/* Task 1: This function retunrs the number of time that 'number' was 
found in the left to right direction */

int leftToRight(int puzzle[SIZE][SIZE], int number){
    int appearancesOfNumber = 0;
    int matches = 0;  //does puzzle[SIZE][SIZE] match int number? start true
    int digits = 0; //get no of digits in number. Ex: if number = 35,  digits = 2. Ex. number = 355,  digits = 3.
    
    if (number == 0) {  //need to do this because (int) floor(log10((float)number)) gives us a weird number when input is 0
        digits = 1;
    }
    
    else {
        digits = (int) floor( log10( (float)number ) ) + 1; 
    }
    
    char numberAsArray[digits];  //make the number to search for into a char array (aka a string)
    
    for (int x = digits-1; x >= 0; x--) { //populate numberAsArray with the individual digits of the number input 
        numberAsArray[x] = number%10;
        number = number/10;
    }
    
    for (int row = 0; row < SIZE; row++) {  //loop through all rows
            for (int column = 0; column < SIZE; column++){ //loop through no. of columns in a row
                    if ((SIZE-column) >= digits) { //if number has 2 digits, there should be min 2 columns left in the row...
                                                       //this preevnts indexing out of array
                        int index = column;
                        matches = 0;  //reset matches for each value of puzzle[SIZE][SIZE]
                        for (int i = 0; i < digits; i++) {  //check this many digits
                            if(puzzle[row][index] == numberAsArray[i]){
                                index++;
                            }
                            else{
                                matches = 1; //matches is false
                            }
                        }
                        if (matches == 0) { //if matches is true
                            appearancesOfNumber++;
                        }
                    }
            }
    }    
    return appearancesOfNumber;
}

/* Task 2: This function rotates the given puzzle 
90 degree to the left. 
a b      becomes     b   B
A B                  a   A
*/
void rotateLeft(int puzzle[SIZE][SIZE]){ //puzzle is a pointer
    // add your code here
    int shiftedPuzzle[SIZE][SIZE];  //new empty int 2-D array, same size as puzzle[][]

    memcpy(shiftedPuzzle, puzzle, sizeof (int) * SIZE * SIZE); //copy puzzle into shiftedPuzzle

    int totalIndex = SIZE - 1;

    for (int i = 0; i < SIZE; i++) { //traverse each row
        int index = totalIndex;
        for (int j = 0; j < SIZE; j++){ //traverse columns in each row
            puzzle[j][i] = shiftedPuzzle[i][index];   //Ex. Assign puzzle[0][0] = shiftedPuzzle[0][3]
            index--;
        } //end of columns in row loop 
    } //end of rows loop

// Below is an example of how this 90 degree shift function works. An * highlights were the change has occured.

//    5 1 7        7* 1 7       7 1 7       7 1 7       7 2* 7       7 2 7       7 2 7       7 2 9*       7 2 9       7 2 9 
//    6 9 2        6 9 2        1* 9 2      1 9 2       1 9 2        1 9* 2      1 9 2       1 9 2        1 9 3*      1 9 3 
//    2 3 9        2 3 9        2 3 9       5* 3 9      5 3 9        5 3 9       5 6* 9      5 6 9        5 6 9       5 6 2*  
  
}

/* Task 3: This function finds the total occurences of 
the number in the puzzle */
int totalOccurrences(int puzzle[SIZE][SIZE], int number){
    int totalOccurencesCount = 0;
    if (number/10 == 0){  //if number to search for has only 1 digit, just search the grid left to right once.
        totalOccurencesCount = leftToRight(puzzle, number);
    }
    else {  //if number has >1 digit, search left to right, rotate 90 degrees, then search left to right again, and so on...
            //for a total of 4 left to right searches. That means the grid was searched left to right, top to bottom, right to left, & bottom to top.
        totalOccurencesCount = leftToRight(puzzle, number);
        rotateLeft(puzzle);
        totalOccurencesCount = totalOccurencesCount + leftToRight(puzzle, number);
        rotateLeft(puzzle);
        totalOccurencesCount = totalOccurencesCount + leftToRight(puzzle, number);
        rotateLeft(puzzle);
        totalOccurencesCount = totalOccurencesCount + leftToRight(puzzle, number); 
    }
    return totalOccurencesCount;
}
