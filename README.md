# Number Search Puzzle

Like a word search, but with numbers. This code searches a 2D grid left to right, top to bottom, right to left, and bottom to top for instances of a given input. Return the number of times the input was found in the grid. Demonstrates my understanding of c programming, memory management model through 2-D arrays, and functionality of the Unix shell.

# Example Inputs and Outputs

/* This tests code with the main function. It is written by me, and the format is inspired by     examples by Professor Marzieh Ahmadzadeh.
*/

int main(){
     setSize(5);  //the grid is size 5 by 5
     int puzzle1[5][5] = {\  //this is the 2D array grid we'll be searching
     {8,1,5,7,3}, 
     {6,8,7,1,0},
     {4,5,1,6,7},
     {5,4,9,9,1},
     {1,2,3,8,5}};

     //Call the totalOccurrences function with different inputs
     printf("%d\n", totalOccurrences(puzzle1, 51));  //prints 4 because we found '51' four times.
     printf("%d\n", totalOccurrences(puzzle1, 50));  //prints 0
     printf("%d\n", totalOccurrences(puzzle1, 1));  //prints 5
     printf("%d\n", totalOccurrences(puzzle1, 2));  //prints 0
     printf("%d\n", totalOccurrences(puzzle1, 719));  //prints 2

     //Testing with unusual inputs
     printf("%d\n", totalOccurrences(puzzle1, 51.0));  //prints 4  so doubles work!
     printf("%d\n", totalOccurrences(puzzle1, 52));  //prints 0
     printf("%d\n", totalOccurrences(puzzle1, 51.9)); //prints 4. Doubles will drop the decimal.

     //characters also accepted!
     printf("%d\n", totalOccurrences(puzzle1, ('d'+'C'))); //prints 1, becase 'd' + 'C' is ASCII 100 + 67 = 167 and 167 appears once in the grid
     printf("%d\n", totalOccurrences(puzzle1, 'A'));  //prints 0 because ASCII value of 'A' is 65
     printf("%d\n", totalOccurrences(puzzle1, '@')); //prints 1 because ASCII value of '@' is 64

     //String inputs
     printf("%d\n", totalOccurrences(puzzle1, "BananaBread")); //no return but no error either
     printf("%d\n", totalOccurrences(puzzle1, (char*)"BananaBread"));  //prints 0

     return 0; 
 }
